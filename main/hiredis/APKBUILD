# Contributor: Eivind Uggedal <eu@eju.no>
# Maintainer: Natanael Copa <ncopa@alpinelinux.org>
pkgname=hiredis
pkgver=1.0.1
pkgrel=0
pkgdesc="Minimalistic C client library for Redis"
url="https://github.com/redis/hiredis"
arch="all"
license="BSD-3-Clause"
makedepends="openssl1.1-compat-dev"
checkdepends="redis"
subpackages="$pkgname-ssl $pkgname-dev"
source="$pkgname-$pkgver.tar.gz::$url/archive/v$pkgver.tar.gz"


build() {
	make USE_SSL=1 PREFIX=/usr DEBUG="$CFLAGS" LDFLAGS="$LDFLAGS"
}

check() {
	redis-server --dir "$builddir" --unixsocket "$builddir"/redis.sock&
	local _redispid=$!

	# make sure socket is available before we start running tests
	local _n=100 # wait up to 10 sec
	while ! [ -e "$builddir"/redis.sock ] && [ $_n -gt 0 ]; do
		sleep 0.1s
		let _n="$_n - 1"
	done

	make hiredis-test
	./hiredis-test -s "$builddir"/redis.sock || (kill $_redispid && false)
	kill $_redispid
}

package() {
	make USE_SSL=1 PREFIX="$pkgdir"/usr install
}

ssl() {
	amove usr/lib/libhiredis_ssl.so*
}

sha512sums="
87909f78171cf4deeb3d030484b55fbd1a7f7f27f33636f90e169a900ddc5c5ec447d2df34739ada3cf35a50d647cb4fcbe2754f4521d18f770f1cf8ed962909  hiredis-1.0.1.tar.gz
"
