From cef3b1f0a6b1965d8f0402b9d515ce7ffa2ce3bd Mon Sep 17 00:00:00 2001
From: Aleix Pol <aleixpol@kde.org>
Date: Tue, 6 Jul 2021 03:13:27 +0200
Subject: [PATCH] Do not create an extra KIconLoader just for custom palettes

Instead use the global one.

Creating a KIconLoader is rather expensive and we were doing it twice
only for setting custom palettes.
---
 .../plasmadesktoptheme.cpp                    | 25 ++++++++++---------
 1 file changed, 13 insertions(+), 12 deletions(-)

diff --git a/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp b/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
index 2881281..d45827e 100644
--- a/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
+++ b/kirigami-plasmadesktop-integration/plasmadesktoptheme.cpp
@@ -18,16 +18,7 @@
 #include <QQmlEngine>
 #include <QQuickRenderControl>
 #include <QQuickWindow>
-
-class IconLoaderSingleton
-{
-public:
-    IconLoaderSingleton() = default;
-
-    KIconLoader self;
-};
-
-Q_GLOBAL_STATIC(IconLoaderSingleton, privateIconLoaderSelf)
+#include <QScopeGuard>
 
 class StyleSingleton : public QObject
 {
@@ -273,9 +264,19 @@ QIcon PlasmaDesktopTheme::iconFromTheme(const QString &name, const QColor &custo
         }
     }
 
-    privateIconLoaderSelf->self.setCustomPalette(pal);
+    bool hadPalette = KIconLoader::global()->hasCustomPalette();
+    QPalette olderPalette = KIconLoader::global()->customPalette();
+
+    auto cleanup = qScopeGuard([&] {
+        if (hadPalette) {
+            KIconLoader::global()->setCustomPalette(olderPalette);
+        } else {
+            KIconLoader::global()->resetPalette();
+        }
+    });
 
-    return KDE::icon(name, &privateIconLoaderSelf->self);
+    KIconLoader::global()->setCustomPalette(pal);
+    return KDE::icon(name, KIconLoader::global());
 }
 
 void PlasmaDesktopTheme::syncColors()
